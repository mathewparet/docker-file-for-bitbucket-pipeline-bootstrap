FROM php:7-fpm

RUN apt-get update && apt-get install -y unzip git curl libmcrypt-dev mysql-client zlib1g-dev
RUN yes | pecl install mcrypt-1.0.1
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install zip
RUN docker-php-ext-enable mcrypt
RUN docker-php-ext-enable zip
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer global require laravel/envoy